﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WplGalgje
{
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window
	{
		string verborgenwoord;
		int aantallevens = 10;
		int lost = 1;
		public MainWindow()
		{
			InitializeComponent();


		}

		private void Button_Click(object sender, RoutedEventArgs e)
		{
			galgjestatus1.Text = "Vul een geheim woord in";
			levens.Text = "10 leven(s)";
			inputverborgenwoord.Text = "";
			aantallevens = 10;
			//knop1.IsEnabled = true;
			knop3.IsEnabled = true;
			letters.Text = "";

			if (verborgenwoord == string.Empty)
			{
				knop1.IsEnabled = false;
			}

		}

		private void Button_Click_1(object sender, RoutedEventArgs e)
		{
			bool leeginput = (inputverborgenwoord.Text == "");

			//levens.Text = $"{aantallevens--} leven(s)";
			verborgenwoord = inputverborgenwoord.Text;
			inputverborgenwoord.Text = "";
			knop1.IsEnabled = true;

			if (inputverborgenwoord.Text == "")
			{
				knop1.IsEnabled = false;
			}




			//Array test = verborgenwoord.ToCharArray();


			if (leeginput)
			{
				galgjestatus1.Text = "Vul een woord in!!";
			}

			else
			{
				galgjestatus1.Text = "Start!";
				knop3.IsEnabled = false;
				knop1.IsEnabled = true;
			}
			
		}

		private void Button_Click_2(object sender, RoutedEventArgs e)
		{
			bool leeginput = inputverborgenwoord.Text == string.Empty;



			if (leeginput)
			{
				galgjestatus1.Text = "Vul een woord in!!";
				//aantallevens = 
			}



			if (inputverborgenwoord.Text == verborgenwoord)
			{
				galgjestatus1.Text = $"Gewonnen! het antwoord was '{verborgenwoord}'";
				knop1.IsEnabled = false;
				knop3.IsEnabled = false;
			}
			
			else
			{
				aantallevens = aantallevens - 1;
				levens.Text = $"{aantallevens} leven(s)";
				//levens.Text = $"{aantallevens --} leven(s)";
				galgjestatus1.Text = "";
				
			}

			if (aantallevens == 0)
			{
				galgjestatus1.Text = $"Je hebt verloren, het antwoord was '{verborgenwoord}'";
				knop1.IsEnabled = false;
				knop3.IsEnabled = false;
			}


			if (aantallevens <= 0)
			{
				aantallevens = lost;
			}

			//Char gegokteletter = Convert.ToChar(inputverborgenwoord.Text);

			//Boolean juistgegokteletter = verborgenwoord.Contains(gegokteletter);

			//if (juistgegokteletter)
			//{
				
			//	letters.Text += inputverborgenwoord.Text;
			//}
			//else
			//{
			//}


		}

		private void inputverborgenwoord_TextChanged(object sender, TextChangedEventArgs e)
		{
			 
		}
	}
}